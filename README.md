# Magic Formula Bot

This is a simple Telegram bot that applies the Magic Formula described by Joel Greenblatt in the book The Little Book That Beats The Market to brazilian stocks.

## **WARNING**
**This application is not up and working because of hosting problems, but the code should work and is open for further deployment.**

## Functionality

The code scrapes the website [Fundamentus](http://fundamentus.com.br/).

It retrieves a DataFrame with data from all the brazilian stocks. Then, it applies two filters:
* <p>The first is a liquidity filter: excludes every ticker that has had no transactions in the last 2 months.</p>
* <p>The second is a P/E (P/L, in portuguese) filter: excludes every ticker that has a non-positive value P/E (filter defined in the referred book).</p>
    
After that, we apply the strategy *per se*:
* <p>First we sort the companies in an ascending way: to the lowest P/E is the first in the rank, and the highest is the last. We attribute ranking position to the ticker.</p>
* <p>Then, we to the same for the ROIC, but this time in an descending way. We attribute this position to a new ranking.
* <p>Finally, we sum the position from both ranks for each company and attribute this to a new column, sorting the companies in an ascending way for the last time.</p>

We have the companies ranked from best to worst in the magic formula's perspective.
We will use the 25 first results from the resulting DataFrame.

This code uses the Flask library to work with the Web side of the process.

We run this code on a [Heroku server](https://magic-formula-bot.herokuapp.com/message) that is hooked to our Telegram Bot.
The commands sent to the bot are posted to this url, that returns the bot's answers.

## How To Use
<p>To use the bot, access [this link](http://t.me/magic_formula_bot).</p>
<p>These are the current possible interactions with the bot:</p>
<p>![/start](https://i.imgur.com/Zi89Tad.png)
![/acoes](https://i.imgur.com/LzWFuUr.png)
![/page2](https://i.imgur.com/cff5jUM.png)</p>

**Do not use these as investment recommendations, much less blindly follow the calls.**

**No certified profissional is behind this bot.**

**This is just an application of a classic financial market formula.**
