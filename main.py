import pandas as pd
import requests
import json
from flask import Flask, request, Response
import os
import bs4

token = '1696889255:AAGK-kSWd3wCu62jUsE6wRvMMr_ARFv8Epo'

# definindo as funções


def get_data():
    # scraping
    
    url = 'http://fundamentus.com.br/resultado.php'
    headers = { 'User-Agent': 'Mozilla/5.0' }
    
    res = requests.get( url, headers=headers )
    
    if res.status_code != 200:
        print('fudeu')
    
    bleb = str(bs4.BeautifulSoup(res.text, 'html.parser'))
    
    # criando o df
    table = pd.read_html(bleb)[0]
    
    # ajustando o df
    table['ROIC'] = table['ROIC'].str.replace('%', '')
    table['ROIC'] = table['ROIC'].str.replace('.', '')
    table['ROIC'] = table['ROIC'].str.replace(',', '.').astype(float)
    
    table['Cotação'] = table['Cotação'] / 100
    
    # aplicando o filtro de liquidez
    table['Liq.2meses'] = table['Liq.2meses'].str.replace('.', '')
    table = table.loc[table['Liq.2meses'].astype(float) != 0.0]
    
    # estreitando a tabela
    table = table.loc[:, ['Papel', 'Cotação', 'P/L', 'ROIC']]
    
    # aplicando o primeiro filtro de Joel: p/l positivo
    table = table.loc[table['P/L'] > 0]
    
    # fazendo o rank por p/l
    table = table.sort_values('P/L', ascending=True).reset_index()
    table['Rank P/L'] = table.index
    
    # aplicando e fazendo o rank do segundo filtro
    table = table.sort_values('ROIC', ascending=False).reset_index()
    table['Rank ROIC'] = table.index
    
    # fazendo o rank final de Joel
    table['Rank Overall'] = table['Rank P/L'] + table['Rank ROIC']
    
    table = table[['Papel', 'Cotação', 'P/L', 'ROIC', 'Rank P/L', 'Rank ROIC', 'Rank Overall']]
    table = table.sort_values('Rank Overall', ascending=True).reset_index()
    
    # deixando só os critérios que usamos
    table = table[['Papel', 'Cotação', 'P/L', 'ROIC', 'Rank P/L', 'Rank ROIC', 'Rank Overall']]
    
    # deixando só as primeiras 25 empresas
    table = table.loc[:24, :]

    # renomeando
    table = table.rename(columns = {'Papel': 'papel', 'Cotação': 'cotacao', 'P/L': 'p_sobre_l', 'ROIC': 'roic', 'Rank P/L': 'rank_p_l', 'Rank ROIC': 'rank_roic',
       'Rank Overall': 'rank_overall'})
    
    return table


def response_start():
    return 'Olá! Eu lhe ajudarei a comprar ações brasileiras recomendadas como no livro A Fórmula Mágica de Joel Greenblatt para Bater o Mercado de Ações.\nPara mais informações, acesse https://gitlab.com/augusto.dp01/bot-magic-formula.\nDisclaimer: Este bot não tem relação com Joel Greenblatt ou qualquer outro profissional de investimentos. Não se trata de recomendação de investimento, apenas aplicação de uma fórmula clássica do mercado financeiro.\nPara ver os principais resultados, digite /acoes.\nPara ver mais alguns resultados, digite /page2.'


def response_acoes():
    
    data = get_data()
    
    response_acoes1 = 'Aqui estão as 10 primeiras ações pelo critério ao fim do último pregão:'
    
    acoes = []
    for i in range(len(data)):
        acoes.append(data.loc[i, 'papel'][:4])
    
    response_acoes2 = ''
    
    i = 0
    for j in range(len(data)):
    
        if acoes[j] not in response_acoes2:
            response_acoes2 = response_acoes2 + '\n' + str(i + 1) + ': ' + acoes[j]
            i += 1
            
        else:
            continue
        
        if i == 10:
            break
        
    return response_acoes1 + response_acoes2


def response_page2():
    
    data = get_data()
    
    acoes = []
    for i in range(len(data)):
        acoes.append(data.loc[i, 'papel'][:4])
        
    page2 = ''
    
    i = 0
    for j in range(len(data)):
    
        if acoes[j] not in page2:
            page2 = page2 + str(i + 1) + ': ' + acoes[j] + '\n' 
            i += 1
            
        else:
            continue
    
    return 'Aqui estão todas as que eu olhei hoje:\n' + page2


def response_n_entendi():

    return 'Perdão, não entendi. Talvez isso não seja um comando.'


def send(token, chat_id, response):
    
    url = 'https://api.telegram.org/bot{}/'.format(token)
    url = url + 'sendMessage?chat_id={}'.format(chat_id)
    
    r = requests.post( url, json={'text': response})
    
    print('Status Code: ' + str(r.status_code))


app = Flask(__name__)

@app.route('/message', methods=['GET', 'POST'])

def do_stuff():
    
    if request.method == 'POST':
        message = request.get_json()
        
        chat_id = message['message']['chat']['id']
        ask = message['message']['text']
    
        if ask == '/start':
            response = response_start()
            
        elif ask == '/acoes':
            response = response_acoes()
            
        elif ask == '/page2':
            response = response_page2()
            
        else:
            response = response_n_entendi()

        send(token, chat_id, response)

        return Response('Ok', status=200)
        
    else:
        return get_data().to_json(orient='records')
    


if __name__ == '__main__':

	port = int(os.environ.get('PORT', 5000))
	app.run(host='0.0.0.0', port=port)
